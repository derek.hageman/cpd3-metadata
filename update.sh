#!/bin/bash

./merge.py acquisition/acquisition.xml | da.archive --remove=none
./merge.py editing/editing.xml | da.archive --remove=none
./merge.py editing/contamination.xml | da.archive --remove=none
./merge.py editing/editdirective.xml | da.archive --remove=none
./merge.py graphing/displays.xml | da.archive --remove=none
./merge.py graphing/update_plots.xml | da.archive --remove=none
./merge.py realtime/realtime.xml | da.archive --remove=none
./merge.py fileoutput/site.xml | da.archive --remove=none
./merge.py fileoutput/ebas.xml | da.archive --remove=none
./merge.py fileoutput/netcdf.xml | da.archive --remove=none
./merge.py fileoutput/sqldb.xml | da.archive --remove=none
./merge.py fileoutput/upload.xml | da.archive --remove=none
./merge.py fileinput/ebas.xml | da.archive --remove=none
./merge.py email/email.xml | da.archive --remove=none
./merge.py misc/tasks.xml | da.archive --remove=none
./merge.py misc/transfer.xml | da.archive --remove=none
./merge.py misc/output_cpd2.xml | da.archive --remove=none
./merge.py transfer/processing.xml | da.archive --remove=none
./merge.py transfer/synchronize.xml | da.archive --remove=none

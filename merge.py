#!/usr/bin/python3

import xml.etree.ElementTree
import sys
import io
import os
import itertools


def merge(file, depth=0):
    if depth > 50:
        raise Exception('Maximum depth exceeded')
    tree = xml.etree.ElementTree.parse(file)
    for node in itertools.chain(tree.findall('[@merge]'), tree.findall('.//*[@merge]')):
        merge_file = node.attrib["merge"]
        merge_file = merge_file.replace('@', os.path.dirname(os.path.realpath(__file__)))
        merge_data = merge(open(merge_file, 'r', encoding='utf-8'), depth+1)
        merge_data = merge_data.getroot()
        node.text = merge_data.text
        node.tail = merge_data.tail
        for k, v in merge_data.attrib.items():
            node.attrib[k] = v
        del node.attrib["merge"]
        for e in reversed(list(merge_data)):
            node.insert(0, e)
    return tree

if len(sys.argv) < 2:
    data = merge(io.TextIOWrapper(sys.stdin.buffer, encoding='utf-8'))
else:
    data = merge(open(sys.argv[1], 'r', encoding='utf-8'))

data.write(sys.stdout.buffer, encoding='utf-8', xml_declaration=True)
<?xml version="1.0" encoding="UTF-8"?>
<value type="metahash">
    <meta key="Editor" type="hash">
        <value key="Type" type="string">AcquisitionComponent</value>
        <value key="Component" type="string">acquire_magee_aethalometer33</value>
    </meta>

    <value key="Area" type="metaarray">
        <meta key="Description" type="string">The areas of both sampling spots</meta>
        <meta key="Count" type="integer">2</meta>
        <meta key="Children" type="metareal">
            <meta key="Description" type="string">The area in mm² of a single sampling spot</meta>
            <meta key="UndefinedDescription" type="string">Automatic or 78.5</meta>
            <meta key="Format" type="string">00.00</meta>
            <meta key="Units" type="string">mm²</meta>
            <meta key="Editor" type="hash">
                <value key="Minimum" type="real">0.1</value>
            </meta>
        </meta>
    </value>

    <value key="Wavelengths" type="metaarray">
        <meta key="Description" type="string">The wavelengths of the measurement channels</meta>
        <meta key="Count" type="integer">7</meta>
        <meta key="Children" type="metareal">
            <meta key="Description" type="string">The central measurement wavelength of the channel</meta>
            <meta key="Units" type="string">nm</meta>
            <meta key="Format" type="string">0</meta>
            <meta key="Editor" type="hash">
                <value key="Minimum" type="real">0.0</value>
                <value key="MinimumExclusive" type="boolean">true</value>
            </meta>
        </meta>
    </value>

    <value key="AbsorptionEfficiency" type="metaarray">
        <meta key="Description" type="string">Absorption efficiency of the measurement channels</meta>
        <meta key="Count" type="integer">7</meta>
        <meta key="Children" type="metareal">
            <meta key="Description" type="string">The absorption efficiency of the channel</meta>
            <meta key="Units" type="string">m²/g</meta>
            <meta key="Format" type="string">0.0</meta>
            <meta key="Editor" type="hash">
                <value key="Minimum" type="real">0.0</value>
                <value key="MinimumExclusive" type="boolean">true</value>
            </meta>
        </meta>
    </value>

    <value key="UseMeasuredTime" type="metastring">
        <meta key="Description" type="string">The behavior used to determine the elapsed time between reports for integration</meta>
        <meta key="Editor" type="hash">
            <value key="Type" type="string">Enumeration</value>
            <value key="DefaultCollapsed" type="boolean">true</value>
        </meta>
        <meta key="EnumerationValues" type="hash">
            <value key="Rounded" type="hash">
                <value key="SortPriority" type="integer">0</value>
                <value key="Text" type="string">Measured and rounded time</value>
                <value key="Description" type="string">Use the measured time between reports rounded to the nearest multiple of the reported time base</value>
            </value>
            <value key="Reported" type="hash">
                <value key="SortPriority" type="integer">1</value>
                <value key="Text" type="string">Reported time base</value>
                <value key="Description" type="string">Use the instrument reported time base</value>
            </value>
            <value key="Measured" type="hash">
                <value key="SortPriority" type="integer">2</value>
                <value key="Text" type="string">Measured time</value>
                <value key="Description" type="string">Use the exact measured time between reports</value>
            </value>
        </meta>
    </value>

    <value key="SetInstrumentTime" type="metaboolean">
        <meta key="Description" type="string">Set the internal clock on the instrument on start up, this can cause faults on the instrument requiring power cycles</meta>
    </value>

    <value key="TotalFlowCalibration" merge="@/common/calibration.xml">
        <meta key="Description" type="string">The calibration applied to the combined total flow</meta>
    </value>

    <value key="Spot1FlowCalibration" merge="@/common/calibration.xml">
        <meta key="Description" type="string">The calibration applied to the flow through spot one</meta>
    </value>

    <value key="Spot2FlowCalibration" merge="@/common/calibration.xml">
        <meta key="Description" type="string">The calibration applied to the flow through spot two</meta>
    </value>

    <value key="DisableEBCZeroCheck" type="metaboolean">
        <meta key="Description" type="string">This disables the check for all EBCs being exactly zero to indicate a tape advance in progress</meta>
    </value>

    <value key="StrictMode" type="metaboolean">
        <meta key="Description" type="string">This causes the system to refuse records that contain extra unrecognized fields</meta>
    </value>

    <value key="RealtimeDiagnostics" type="metaboolean">
        <meta key="Description" type="string">Enable all realtime data diagnostic generation, resulting in a greatly increased amount of data generated</meta>
    </value>


    <value key="Leakage" type="metareal">
        <meta key="Description" type="string">The leakage factor (ζ) to use and set on the instrument</meta>
        <meta key="UndefinedDescription" type="string">Read on startup</meta>
        <meta key="Format" type="string">0.00</meta>
        <meta key="Editor" type="hash">
            <value key="Minimum" type="real">0.0</value>
            <value key="AllowUndefined" type="boolean">true</value>
        </meta>
    </value>

    <value key="WeingartnerConstant" type="metareal">
        <meta key="Description" type="string">The Weingartner constant (C) to use and set on the instrument</meta>
        <meta key="UndefinedDescription" type="string">Read on startup</meta>
        <meta key="Format" type="string">0.00</meta>
        <meta key="Editor" type="hash">
            <value key="Minimum" type="real">0.0</value>
            <value key="AllowUndefined" type="boolean">true</value>
        </meta>
    </value>

    <value key="ATNf1" type="metareal">
        <meta key="Description" type="string">The lower attenuation threshold to use and set on the instrument used to determine the start of FVRF calculation</meta>
        <meta key="UndefinedDescription" type="string">Read on startup</meta>
        <meta key="Format" type="string">0</meta>
        <meta key="Editor" type="hash">
            <value key="Minimum" type="real">0.0</value>
            <value key="AllowUndefined" type="boolean">true</value>
            <value key="MinimumExclusive" type="boolean">true</value>
        </meta>
    </value>

    <value key="ATNf2" type="metareal">
        <meta key="Description" type="string">The second attenuation threshold to use and set on the instrument used to determine the end of FVRF calculation and the beginning of new-constant application</meta>
        <meta key="UndefinedDescription" type="string">Read on startup</meta>
        <meta key="Format" type="string">0</meta>
        <meta key="Editor" type="hash">
            <value key="Minimum" type="real">0.0</value>
            <value key="AllowUndefined" type="boolean">true</value>
            <value key="MinimumExclusive" type="boolean">true</value>
        </meta>
    </value>

    <value key="kMin" type="metareal">
        <meta key="Description" type="string">The lower bound of accepted correction constants to use and set on the instrument</meta>
        <meta key="UndefinedDescription" type="string">Read on startup</meta>
        <meta key="Format" type="string">0.000000</meta>
        <meta key="Editor" type="hash">
            <value key="AllowUndefined" type="boolean">true</value>
        </meta>
    </value>

    <value key="kMax" type="metareal">
        <meta key="Description" type="string">The upper bound of accepted correction constants to use and set on the instrument</meta>
        <meta key="UndefinedDescription" type="string">Read on startup</meta>
        <meta key="Format" type="string">0.000000</meta>
        <meta key="Editor" type="hash">
            <value key="AllowUndefined" type="boolean">true</value>
        </meta>
    </value>

    <value key="TimeBase" type="metainteger">
        <meta key="Description" type="string">The instrument calculation and averaging time</meta>
        <meta key="Units" type="string">s</meta>
        <meta key="UndefinedDescription" type="string">Read on startup</meta>
        <meta key="Editor" type="hash">
            <value key="Minimum" type="integer">1</value>
        </meta>
    </value>

</value>
